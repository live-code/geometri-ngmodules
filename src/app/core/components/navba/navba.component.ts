import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navba',
  template: `

    <button routerLink="login">login</button>
    <button routerLink="home">home</button>
    <button routerLink="catalog">catalog</button>
    <button routerLink="contacts">contacts</button>

  `,
  styles: [
  ]
})
export class NavbaComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
