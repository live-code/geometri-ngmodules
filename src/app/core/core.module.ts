import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbaComponent } from './components/navba/navba.component';
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [
    NavbaComponent
  ],
  exports: [
    NavbaComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class CoreModule { }
