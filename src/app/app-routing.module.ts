import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {ContactsComponent} from "./features/contacts/contacts.component";

@NgModule({
  imports: [
    RouterModule.forRoot([
      {path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule)},
      {path: 'catalog', loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule)},
      {path: 'contacts', component: ContactsComponent},
      {path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule)},
      {path: '', redirectTo: 'home', pathMatch: 'full'},
      { path: 'uikit', loadChildren: () => import('./features/uikit/uikit.module').then(m => m.UikitModule) },
    ])
  ],
  exports: [
    RouterModule,
  ]
})
export class AppRoutingModule {}
