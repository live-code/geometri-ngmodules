import {NgModule} from "@angular/core";
import {CardComponent} from "./components/card.component";
import { MapquestComponent } from './components/mapquest.component';
import { TabbarComponent } from './components/tabbar.component';
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {CardModule} from "./components/card.module";
import {TabbarModule} from "./components/tabbar.module";

@NgModule({
  declarations: [

    MapquestComponent,
  ],
  imports: [
    CommonModule,
    TabbarModule,
    FormsModule,
    CardModule,
  ],
  exports: [
    CardModule,
    MapquestComponent,
    TabbarComponent,
    TabbarModule,
  ]
})
export class SharedModule {}
