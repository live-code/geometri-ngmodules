import {NgModule} from "@angular/core";
import {HomeComponent} from "./home.component";
import {HeroComponent} from "./components/hero.component";
import {NewsletterComponent} from "./components/newsletter.component";
import {NewsComponent} from "./components/news.component";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import { HelpComponent } from './help.component';
import {CardModule} from "../../shared/components/card.module";

@NgModule({
  declarations: [
    HomeComponent,
    HeroComponent,
    NewsletterComponent,
    NewsComponent,
    HelpComponent,
  ],
  imports: [
    FormsModule,
    CardModule,
    CommonModule,
    RouterModule.forChild([
      { path: 'help', component: HelpComponent},
      { path: '', component: HomeComponent}
    ])
  ]
})
export class HomeModule {

}
