import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';
import {TabbarModule} from "../../shared/components/tabbar.module";


const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    children: [
      { path: 'signin', loadChildren: () => import('./pages/signin/signin.module').then(m => m.SigninModule) },
      { path: 'registration', loadChildren: () => import('./pages/registration/registration.module').then(m => m.RegistrationModule) },
      { path: 'lostpass', loadChildren: () => import('./pages/lostpas/lostpas.module').then(m => m.LostpasModule) },
      { path: '', redirectTo: 'signin', pathMatch: 'full'}

    ]
  },

];

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TabbarModule
  ]
})
export class LoginModule { }
