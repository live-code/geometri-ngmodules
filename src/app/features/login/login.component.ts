import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  template: `
    <h1>Login</h1>

    <router-outlet></router-outlet>

    <hr>

    <app-tabbar></app-tabbar>
    <button routerLink="signin">Signin</button>
    <button routerLink="registration">regi</button>
    <button routerLink="/login/lostpass">lostpass</button>
  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
