import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LostpasComponent } from './lostpas.component';


const routes: Routes = [
  { path: '', component: LostpasComponent }
];

@NgModule({
  declarations: [
    LostpasComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class LostpasModule { }
