import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CatalogComponent} from "./catalog.component";
import {RouterModule} from "@angular/router";
import { ProductsComponent } from './pages/products.component';
import { OffersComponent } from './pages/offers.component';
import { GroupsComponent } from './pages/groups.component';
import {SharedModule} from "../../shared/shared.module";



@NgModule({
  declarations: [
    CatalogComponent,
    ProductsComponent,
    OffersComponent,
    GroupsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: CatalogComponent,
        children: [
          {path: 'offers', component: OffersComponent},
          {path: 'products', component: ProductsComponent},
          {path: 'groups', component: GroupsComponent},
          {path: '', redirectTo: 'offers', pathMatch: 'full'}
        ]
      },

    ]),
  ]
})
export class CatalogModule { }
