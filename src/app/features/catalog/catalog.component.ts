import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-catalog',
  template: `

    <button routerLink="offers">offerte</button>
    <button routerLink="products">prodotti</button>
    <button routerLink="groups">gruppi</button>
    <app-card></app-card>

    <router-outlet></router-outlet>

    <footer>footer bla bla</footer>


  `,
  styles: [
  ]
})
export class CatalogComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
