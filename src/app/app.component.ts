import { Component } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-root',
  template: `
    <h1>Hello Geometri</h1>

    <app-navba></app-navba>
    <hr>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {
}
